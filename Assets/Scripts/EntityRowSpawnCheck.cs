using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityRowSpawnCheck : MonoBehaviour
{
    private Vector2 overlapBoxBounds;
    private Transform myTransform;
    [SerializeField] private LayerMask overlapLayerMask;


    public void InitSpawnChecker(float yPos, float boundsWidth)
    {
        Vector2 newSize = new Vector2(boundsWidth, 1.0f);
        overlapBoxBounds = newSize;

        myTransform = this.transform;
        
        gameObject.transform.position = new Vector3(0.0f, yPos, 0.0f);

        StartCoroutine(CheckForOverlaps());

        myTransform.localScale = new Vector3(newSize.x, newSize.y, 1.0f);
    }

    public void RepositionRowSpawnChecker(float posY, float entityHeight)
    {
        if (myTransform == null)
        {
            myTransform = this.transform;
        }

        Vector3 newPos = myTransform.position;
        newPos.y = posY;
        myTransform.position = newPos;
        overlapBoxBounds.y = entityHeight;

        //test
        Vector3 _t = myTransform.localScale;
        _t.y = entityHeight;
        myTransform.localScale = _t;
    }

    public void ToggleSpawnCheck(bool state)
    {
        if (state == true)
        {
            StartCoroutine(CheckForOverlaps());
        }
        else
        {
            StopAllCoroutines();
        }
    }

    IEnumerator CheckForOverlaps()
    {
        yield return new WaitUntil(() => GameManager.curGameManager != null);
        while (true)
        {
            AbleToSpawnNextCheck();
            yield return new WaitForSeconds(0.1f);
        }
    }

    void AbleToSpawnNextCheck()
    {
        GameManager.curGameManager.CanSpawnNextRow = !Physics2D.OverlapBox(myTransform.position, overlapBoxBounds, 0.0f, overlapLayerMask);
    }
    
}
