﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct EntitySpawnInfo
{
    public int spnPos;
    public EntityTypes entityType;
}

public class LevelSpawnData : MonoBehaviour {

    //Queue<EntitySpawnInfo> nextSpawnData;
    int columns = 6; //, maxEntitiesPerRow = 3;
    public EntitySpawner entitySpawner;
    public List<List<int>> rowData;
    List<EntitySpawnInfo> tempSpawnData;
    int curDataIndex;
    int spawnPositionCount;
    private List<int> tempSpawnPositions;
    private int spawnCountSincePickup = 0;

    public int CurrentDataIndex
    {
        get { return curDataIndex; }
        set { curDataIndex = value; }
    }

    public int Columns
    {
        get { return columns; }
        set { columns = value; }
    }

    //public int MaxEntitiesPerRow
    //{
    //    get { return maxEntitiesPerRow; }
    //    set { maxEntitiesPerRow = value; }
    //}
    
    public void InitSpawnData(int spawnPosCount)
    {
        Random.InitState((int)(Time.realtimeSinceStartup * 100));

        spawnPositionCount = spawnPosCount;

        tempSpawnPositions = new List<int>();
        for (int i = 0; i < spawnPosCount; i++)
        {
            tempSpawnPositions.Add(i);
        }
    }

    //public void GenerateSpawnPositionData(Vector3[] spawnPosArray)
    //{
    //    Random.InitState((int)(Time.realtimeSinceStartup * 100));

    //    spawnPositionCount = spawnPosArray.Length;

    //    rowData = new List<List<int>>();

    //   // Vector3[] spawnPosRef = spawnPosArray; // entitySpawner.spawnPositions;
    //     // entitySpawner.spawnPositions.Length;
        

    //    for (int i = 0; i < spawnPosArray.Length; i++) 
    //    {
    //        List<int> spawnPosCount = new List<int>(spawnPositionCount);
    //        for (int y = 0; y < spawnPositionCount; y++)
    //        {
    //            spawnPosCount.Add(y);
    //        }

    //        for (int x = 0; x < spawnPosCount.Count; x++)
    //        {
    //            int temp = spawnPosCount[x];
    //            int _r = Random.Range(x, spawnPosCount.Count);                
    //            spawnPosCount[x] = spawnPosCount[_r];
    //            spawnPosCount[_r] = temp;
    //        }
    //        rowData.Add(spawnPosCount);
    //    }

    //}

    public List<EntitySpawnInfo> ReturnNextEntityRowSpawnData(int entitiesPerRow)
    {
        List<EntitySpawnInfo> _spawnData = new List<EntitySpawnInfo>();

        //randomise spawn positions
        for (int x = 0; x < tempSpawnPositions.Count; x++)
        {
            int _temp = tempSpawnPositions[x];
            int _r = Random.Range(x, tempSpawnPositions.Count);
            tempSpawnPositions[x] = tempSpawnPositions[_r];
            tempSpawnPositions[_r] = _temp;
        }

        for (int i = 0; i < entitiesPerRow; i++)
        {

            _spawnData.Add(new EntitySpawnInfo
            {
                spnPos = tempSpawnPositions[i],
                entityType = GenerateNextSpawnEntity()
            });
        }
        

        return _spawnData;

    }

    //public List<EntitySpawnInfo> ReturnNextEntityRowSpawnData()
    //{
    //    tempSpawnData = new List<EntitySpawnInfo>(maxEntitiesPerRow);
        
    //    if (curDataIndex >= rowData.Count)
    //    {
    //        curDataIndex = Random.Range(0, rowData.Count / 2);
    //    }
        
    //    for (int i = 0; i < rowData[curDataIndex].Count; i++)
    //    {
            
    //        tempSpawnData.Add(new EntitySpawnInfo
    //        {
    //            spnPos = rowData[curDataIndex][i],
    //            entityType = GenerateNextSpawnEntity()
    //        });
    //    }

    //    curDataIndex++;
        
    //    return tempSpawnData;

    //}

    private EntityTypes GenerateNextSpawnEntity()
    {
        EntityTypes _type = EntityTypes.CAR;

        if (spawnCountSincePickup > 10) //Just make to sure player get some ammo
        {
            spawnCountSincePickup = 0;
            return EntityTypes.PICKUP;
        }
        
        switch (Random.Range(0, 30))
        {
            case int n when n <= 1:
                _type = EntityTypes.PICKUP;
                spawnCountSincePickup = 0;
                break;
            case int n when n > 2:
                spawnCountSincePickup++;
                break;
            default:
                _type = EntityTypes.CAR;
                spawnCountSincePickup++;
                break;
        }

        return _type;

    }

   
}
