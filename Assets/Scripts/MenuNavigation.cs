﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuNavigation : MonoBehaviour
{
    private AsyncOperation asyncLoad;
    [SerializeField] private TextMeshProUGUI playButtonText;
    [SerializeField] private Color loadingTextColour, playTextColour;

    void Start()
    {
        playButtonText.fontSize = 80.0f;
        playButtonText.color = loadingTextColour;
        playButtonText.text = "Loading..";
        StartCoroutine(LoadMainLevelAsync());
    }

    IEnumerator LoadMainLevelAsync()
    {
        asyncLoad = SceneManager.LoadSceneAsync(1);
        asyncLoad.allowSceneActivation = false;
        
        while (asyncLoad.progress <= 0.85f)
        {
            playButtonText.text = "Loading.. " + (asyncLoad.progress * 100).ToString() + "%";
            yield return null;
        }

        playButtonText.text = "Loading.. " + (asyncLoad.progress * 100).ToString() + "%";

        yield return new WaitForSeconds(0.5f);
        playButtonText.fontSize = 110.0f;
        playButtonText.color = playTextColour;
        playButtonText.text = "Play";
    }

    public void Navigate(int _val)
    {
        switch (_val)
        {
            case 0:
               asyncLoad.allowSceneActivation = true;
                //SceneManager.LoadScene(1);
                break;
            default:
                break;
        }
    }
    
}
