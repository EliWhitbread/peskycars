﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Entity : MonoBehaviour {

    [SerializeField] int health, scoreVal;
    [SerializeField] float moveSpeed, killPosY, offScreenY, colliderBoundsY;
    Transform myTransform;
    HpTextMover textMover;
    bool canSpawnNextEntity = true;
    private float hitSoundPitchAdjAmt;
    bool bIsInitialLevelSpawn = false;

    //can this entity's position be used to spawn the next
    public bool CanSpawnNextEntity
    {
        set { canSpawnNextEntity = value; }
    }
    
    public float MoveSpeed
    {
        set { moveSpeed = value; }
    }

    public float OffScreenY
    {
        set => offScreenY = value;
    }

    public bool IsInitialLevelSpawn
    {
        set => bIsInitialLevelSpawn = value;
    }

    private void OnEnable()
    {
        if (myTransform == null)
        {
            myTransform = this.transform;
        }

        if (colliderBoundsY == 0.0f)
        {
            colliderBoundsY = gameObject.GetComponent<Collider2D>().bounds.size.y / 2;
        }

        if(textMover == null)
        {
            textMover = GetComponentInChildren<HpTextMover>();
        }
        textMover.UpdateText(health); //update floating TextMesh (child GameObject)

        EntitySpawner.OnEntitySpeedUpdate += EntitySpawner_OnEntitySpeedUpdate;

        hitSoundPitchAdjAmt = 0.7f / health;

    }

    private void OnDisable()
    {
        EntitySpawner.OnEntitySpeedUpdate -= EntitySpawner_OnEntitySpeedUpdate;
    }

    private void EntitySpawner_OnEntitySpeedUpdate(float val)
    {
        moveSpeed = val;
    }

    //entity Hit Points
    public int Health
    {
        get { return health; }
        set { health = value; }
    }

    /// <summary>
    /// Apply damage to the Entity - Disable when Health == 0
    /// </summary>
    /// <param name="amt">Damage Amount</param>
	public void TakeDamage(int amt)
    {
        AudioManager.audioManager.PlayAudioOneShot(AudioClipSelection.BallHit, 1.5f - (hitSoundPitchAdjAmt * health));

        health -= amt;
        textMover.UpdateText(health);
        if (health <= 0)
        {
            GameManager.curGameManager.UpdatePlayerScore(scoreVal);
            if (bIsInitialLevelSpawn)
            {
                GameManager.curGameManager.RemoveInitialEntityFromPool(this);
            }
            int _rnd = Random.Range(0, 8);
            if(_rnd == 0)
            {
                GameManager.curGameManager.SpawnPickup(PickupType.AMMO, myTransform.position);
            }
            gameObject.SetActive(false);
            return;
        }

        
        //GameManager.curGameManager.UpdatePlayerScore(amt);
    }

    private void Update()
    {
        myTransform.Translate(-Vector2.up * moveSpeed * Time.deltaTime);
        //if(canSpawnNextEntity == true && myTransform.position.y <= spawnNextPosY)
        //{
        //    canSpawnNextEntity = false;
        //    GameManager.curGameManager.SpawnNextEntity();
        //}
        if(myTransform.position.y <= offScreenY - colliderBoundsY)
        {
            GameManager.curGameManager.UpdatePlayerScoreMutiplier(-1);
            gameObject.SetActive(false);
        }
    }
    
}
