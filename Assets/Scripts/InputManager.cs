﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player input manager
/// </summary>
public class InputManager : MonoBehaviour {

    public delegate void PlayerTouch(Vector3 touchPos);
    public static event PlayerTouch OnTouchDown;
    public static event PlayerTouch OnTouchUp;
    public static event PlayerTouch OnTouchMove;

    bool touchDetected = false;
    Camera mCamera;
   // Vector3 touchPos = Vector3.zero;

    private void Start()
    {
        mCamera = Camera.main;
    }

    private void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN

        //touchPos = mCamera.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (OnTouchDown != null)
            {
                OnTouchDown(mCamera.ScreenToWorldPoint(Input.mousePosition));
                touchDetected = true;
            }
        }
        if (touchDetected == true)
        {
            if (OnTouchMove != null)
            {
                OnTouchMove(mCamera.ScreenToWorldPoint(Input.mousePosition));
            }
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            if (OnTouchUp != null)
            {
                OnTouchUp(mCamera.ScreenToWorldPoint(Input.mousePosition));
                touchDetected = false;
            }
        }

#endif

#if UNITY_IOS || UNITY_ANDROID

        if(Input.touchCount > 0)
        {
            if(Input.touches[0].phase == TouchPhase.Began)
            {
                if (OnTouchDown != null)
                {
                    OnTouchDown(mCamera.ScreenToWorldPoint(Input.touches[0].position));
                }
            }
            if (Input.touches[0].phase == TouchPhase.Moved)
            {
                if (OnTouchMove != null)
                {
                    OnTouchMove(mCamera.ScreenToWorldPoint(Input.touches[0].position));
                }
            }
            if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                if (OnTouchUp != null)
                {
                    OnTouchUp(mCamera.ScreenToWorldPoint(Input.touches[0].position));
                }
            }
        }


        //if(touchDetected == false && Input.touchCount > 0 )
        //{
        //    if(OnTouchDown != null)
        //    {
        //        OnTouchDown(mCamera.ScreenToWorldPoint(Input.touches[0].position));
        //    }
        //    touchDetected = true;
        //}
        //if(touchDetected == true && Input.touchCount < 1)
        //{
        //    if(OnTouchUp != null)
        //    {
        //        OnTouchUp(mCamera.ScreenToWorldPoint(Input.touches[0].position));
        //    }
        //    touchDetected = false;
        //}

#endif
    }


}
