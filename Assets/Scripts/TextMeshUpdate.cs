﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextMeshUpdate : MonoBehaviour {

    [SerializeField] TextMeshPro _tMesh;
     

    int textValue;

    /// <summary>
    /// update the player's projectile count display
    /// </summary>
    public int TextValue
    {
        set { textValue += value; UpdateText(textValue); }
    }


	void Start () {

        _tMesh = gameObject.GetComponent<TextMeshPro>();
	}
	
	void UpdateText(int amt)
    {
        if(_tMesh == null) // && gameObject.activeInHierarchy)
        {
            return;
            //_tMesh = gameObject.GetComponent<TextMeshPro>();
        }
        _tMesh.text = amt.ToString();
    }
}
