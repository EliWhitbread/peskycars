using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathIndicatorElement : MonoBehaviour
{
    private Transform indicatorTransform;

    private SpriteRenderer rnd;

    [SerializeField] private Color canShootColour = Color.white, canNotShootColour = Color.red;
    private bool bCurrentCanShootState;

    void OnEnable()
    {
        if (indicatorTransform == null)
        {
            indicatorTransform = this.transform;
        }

        if (rnd == null)
        {
            rnd = gameObject.GetComponent<SpriteRenderer>();
        }

        bCurrentCanShootState = true;
        rnd.color = canShootColour;
    }

    public void UpdateElementPosition(Vector3 pos)
    {
        indicatorTransform.position = pos;
    }

    public void UpdateCanShoot(bool value)
    {
        if(value == bCurrentCanShootState) return;

        bCurrentCanShootState = value;
        rnd.color = bCurrentCanShootState ? canShootColour : canNotShootColour;
    }
}
