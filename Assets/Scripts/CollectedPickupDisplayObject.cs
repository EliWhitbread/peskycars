using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectedPickupDisplayObject : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    private Transform myTransform;
    private Vector2 destination = Vector2.zero;
    private Vector3 endScale = new Vector3(0.3f, 0.3f, 0.3f);
    private float lerpStep;
    private PickupType pickupType;
    private int puValue;

    public void InitPickupProxy(PickupType puType, int puVal)
    {
        destination = GameManager.curGameManager.AmmoDisplayUIPosition;

        myTransform = this.transform;
        myTransform.localScale = Vector3.one;

        lerpStep = moveSpeed / 10.0f;

        pickupType = puType;
        puValue = puVal;
    }

    // Update is called once per frame
    void Update()
    {
        if(destination == Vector2.zero ) {return;}

        myTransform.localScale = Vector3.Lerp(myTransform.localScale, endScale, lerpStep * Time.deltaTime);

        myTransform.position = Vector3.MoveTowards(myTransform.position, destination, moveSpeed * Time.deltaTime);

        if (Vector3.Distance(myTransform.position, destination) <= 0.2f)
        {
            GameManager.curGameManager.CollectedPickup(pickupType, puValue);
            Destroy(this.gameObject);
        }
    }
}
