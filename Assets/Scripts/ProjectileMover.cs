﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMover : MonoBehaviour {

    Transform myTransform, player;
    Rigidbody2D rBody;
    private ProjectileState projectileState;
    private float stuckTime = 0.0f;

    [SerializeField]bool dead = false; //armed = false
    [SerializeField]float playerTestDistance = 0.015f, deadMoveSpeed = 10.0f;

    float moveForce, refXVelocity;
    [SerializeField]int damagePerCollision = 1, deadBounces = 0;

    //colour
    private SpriteRenderer sRenderer;
    [SerializeField] private Color baseColour, deadColour, boostColour;

    /// <summary>
    /// projectile initial velocity
    /// </summary>
    public float MoveForce
    {
        set { moveForce = value; }
    }

    /// <summary>
    /// active in the scene but returning to spawner/player
    /// </summary>
    //public bool Dead
    //{
    //    set { dead = value; OnProjectileStateChange(ProjectileState.Dead); }
    //}

    public ProjectileState ProjectileState
    {
        get => projectileState;
        set { projectileState = value; OnProjectileStateChange(value); }
    }

    public void InitProjectile()
    {
        if (rBody == null)
        {
            rBody = gameObject.GetComponent<Rigidbody2D>();
        }
        rBody.simulated = false;
        if (myTransform == null)
        {
            myTransform = this.transform;
        }
        if(player == null)
        {
            try
            {
                player = GameObject.FindGameObjectWithTag("Player").transform;
            }
            catch (System.Exception)
            {

                gameObject.SetActive(false);
                return;
            }
            
        }

        sRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    public void Fire()
    {
        OnProjectileStateChange(ProjectileState.Alive);
        //dead = false;
        //armed = true;
        deadBounces = 0;
        rBody.simulated = true;
        stuckTime = 0.0f;

        //rBody.velocity = myTransform.up * moveForce;
        rBody.AddForce(myTransform.up * moveForce, ForceMode2D.Impulse);
        ProjectileSpawner.curProjectileSpawner.ActiveProjectiles = 1;
    }

    void OnProjectileStateChange(ProjectileState newState)
    {
        if (sRenderer == null)
        {
            sRenderer = gameObject.GetComponent<SpriteRenderer>();
        }

        switch (newState)
        {
            case ProjectileState.Alive:
                dead = false;
                sRenderer.color = baseColour;
                break;
            case ProjectileState.Dead:
                if (player == null)
                {
                    player = GameObject.FindGameObjectWithTag("Player").transform;
                }
                dead = true;
                sRenderer.color = deadColour;
                break;
            case ProjectileState.Boost:
                dead = false; //just in case
                sRenderer.color = boostColour;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }
    }

    private void OnDisable()
    {
        //if(armed == false)
        //{
        //   return;
        //}
        //ProjectileSpawner.curProjectileSpawner.textUpdater.TextValue = 1; //update the available projectiles UI text
        //ProjectileSpawner.curProjectileSpawner.WaitingProjectiles = 1; //update "waiting projectiles" - player cannot fire until all projectiles are returned and waiting
        
        ProjectileSpawner.curProjectileSpawner.ProjectileReturnedToPool();
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        stuckTime = 0.0f;
        if(dead == true)
        {
            return;
        }

        //if (armed == true)
        //{
        //}

        if (collision.collider.CompareTag("Floor")) // || collision.collider.CompareTag("Player")) //Return projectiles that hit the "floor" or the player
        {
            rBody.velocity = Vector2.zero;
            //dead = true;
            OnProjectileStateChange(ProjectileState.Dead);
            return;
        }

        Entity entity = collision.gameObject.GetComponent<Entity>(); //do damage to hit Entities
        if(entity != null)
        {
            //AudioManager.audioManager.PlayAudioOneShot(AudioClipSelection.BallHit, 0.5f);
            deadBounces = 0;
            entity.TakeDamage(damagePerCollision);
            return;
        }

        //AudioManager.audioManager.PlayAudioOneShot(AudioClipSelection.BallHit);
        deadBounces++;

        if(deadBounces >= 10)
        {
            OnProjectileStateChange(ProjectileState.Dead);
            return;
        }

        //if(myTransform.position.y == refXVelocity) //stop the projectile from bouncing endlessly between walls
        //{
        //    deadBounces++;
        //    if(deadBounces >= 2)
        //    {
        //        //dead = true;
        //        OnProjectileStateChange(ProjectileState.Dead);
        //        return;
        //    }

        //    //rBody.velocity -= Vector2.up * Random.Range(-moveForce / 2, moveForce / 2);
        //}
        //refXVelocity = myTransform.position.y;

    }

    private void OnCollisionStay2D(Collision2D other)
    {
        stuckTime += Time.deltaTime;

        if (stuckTime >= 0.3f)
        {
            stuckTime = 0.0f;
            OnProjectileStateChange(ProjectileState.Dead);
        }
    }
    //private void OnTriggerExit2D(Collider2D collision) //arm the projectile when it exits the player trigger 
    //{
    //    if(armed == false)
    //    {
    //        armed = true;
    //        ProjectileSpawner.curProjectileSpawner.ActiveProjectiles = 1;
    //    }
    //}

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if(armed == false)
    //    {
    //        return;
    //    }
    //    if (collision.CompareTag("Player"))
    //    {
    //        rBody.velocity = Vector2.zero;
    //        dead = true;
    //    }
    //}

    private void Update()
    {
        if (dead == true)
        {
            rBody.simulated = false;

            float _step = Mathf.Max(deadMoveSpeed, (player.position - myTransform.position).sqrMagnitude) * Time.deltaTime;

            myTransform.position = Vector2.MoveTowards(myTransform.position, player.position, _step);

            Vector2 offset = player.position - myTransform.position;
            float sqrLength = offset.sqrMagnitude;

            if(sqrLength < playerTestDistance * playerTestDistance)
            {
                gameObject.SetActive(false);
                
            }
        }
    }
    
}
