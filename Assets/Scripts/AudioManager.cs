using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum AudioClipSelection
{
    BallHit,
    CarCrash,
    CarHorn
}

public class AudioManager : MonoBehaviour
{
    public static AudioManager audioManager { get; private set; }

    [SerializeField]private AudioClip ballHitAudio, carCrashAudio, hornAudio;
    [SerializeField] private AudioSource fXAudioSource;
    //private float ballHitVolume = 1.0f, carCrashVolume = 0.9f, hornVolume = 0.8f;
    [SerializeField] private List<AudioSource> ballAudioSources;
    private int maxAudioSources = 6;
    [SerializeField]private AudioMixer aMixer;
    private AudioMixerGroup masterMixerGroup, ballsMixerGroup, fXMixerGroup;
    private bool isMuteInGame;
    private PersistentDataManager dataManager;
    private AudioSettingsData audioVolumeData;

    public AudioSettingsData AudioVolumeData
    {
        get => audioVolumeData;
    }

    public bool IsMuteInGame
    {
        get => isMuteInGame;
        set => isMuteInGame = value;
    }

    private void Awake()
    {
        if (audioManager != null && audioManager != this)
        {
            Destroy(this);
        }
        else
        {
            audioManager = this;
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        SetupAudioSources();

        if (ballHitAudio == null)
        {
            ballHitAudio = Resources.Load<AudioClip>("Audio/BallHit_01");
        }
        if (carCrashAudio == null)
        {
            carCrashAudio = Resources.Load<AudioClip>("Audio/Crash_01");
        }
        if(hornAudio == null)
        {
            hornAudio = Resources.Load<AudioClip>("Audio/HornDouble_01");
        }

        dataManager = GameManager.curGameManager.transform.gameObject.GetComponent<PersistentDataManager>();

        RefreshVolumeSettings();
    }

    public void RefreshVolumeSettings()
    {
        audioVolumeData = dataManager.GetAudioSettingsData();
        //isMuteInGame = aData.isMuted;
        //ballHitVolume = aData.ballVolume;
        //carCrashVolume = aData.crashVolume;
        //hornVolume = aData.notificationVolume;
    }

    void SetupAudioSources()
    {
        if (aMixer == null)
        {
            aMixer = Resources.Load<AudioMixer>("Audio/MasterAudioMixer");
        }

        AudioMixerGroup[] _amGroup = aMixer.FindMatchingGroups("Master");
        
        masterMixerGroup = _amGroup[0];
        ballsMixerGroup = _amGroup[1];
        fXMixerGroup = _amGroup[2];
        
        if (fXAudioSource == null)
        {
            fXAudioSource = gameObject.transform.GetChild(0).GetComponentInChildren<AudioSource>();
            fXAudioSource.outputAudioMixerGroup = fXMixerGroup;
            fXAudioSource.volume = 1.0f;
        }

        if (ballAudioSources != null && ballAudioSources.Count == maxAudioSources)
        {
            return;
        }

        ballAudioSources = new List<AudioSource>();

        for (int i = 0; i < maxAudioSources; i++)
        {
            AudioSource _as = gameObject.AddComponent<AudioSource>();
            _as.spatialBlend = 0;
            _as.bypassReverbZones = true;
            _as.playOnAwake = false;
            _as.volume = 1.0f;
            _as.outputAudioMixerGroup = ballsMixerGroup;
            ballAudioSources.Add(_as);
        }
    }

    public void PlayAudioOneShot(AudioClipSelection selection, float forcePitch = 1.0f)
    {
        if(isMuteInGame) return;

        switch (selection)
        {
            case AudioClipSelection.BallHit:
                for (int i = 0; i < ballAudioSources.Count; i++)
                {
                    if (!ballAudioSources[i].isPlaying)
                    {
                        ballAudioSources[i].pitch = forcePitch;
                        ballAudioSources[i].PlayOneShot(ballHitAudio, audioVolumeData.ballVolume);
                        return;
                    }
                }
                break;
            case AudioClipSelection.CarCrash:
                //fXAudioSource.volume = carCrashVolume;
                fXAudioSource.PlayOneShot(carCrashAudio, audioVolumeData.crashVolume);
                break;
            case AudioClipSelection.CarHorn:
                for (int i = 0; i < ballAudioSources.Count; i++)
                {
                    if (!ballAudioSources[i].isPlaying)
                    {
                        ballAudioSources[i].pitch = forcePitch;
                        //ballAudioSources[i].volume = audioVolumeData.notificationVolume;
                        ballAudioSources[i].PlayOneShot(hornAudio, audioVolumeData.notificationVolume);
                        return;
                    }
                }
                fXAudioSource.pitch = forcePitch;
                //fXAudioSource.volume = hornVolume;
                fXAudioSource.PlayOneShot(hornAudio, audioVolumeData.notificationVolume);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(selection), selection, null);
        }

    }
    
}
