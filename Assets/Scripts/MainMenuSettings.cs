using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuSettings : MonoBehaviour
{
    private PersistentDataManager dataManager;
    private GameObject settingsPanel;
    private Button backButton;
    [SerializeField] private Slider ballVolumeSlider, crashVolumeSlider, notificationVolumeSlider;
    private bool settingsChanged;
    private AudioSettingsData audioData;
    
    
    void Start()
    {
        dataManager = gameObject.GetComponent<PersistentDataManager>();
        audioData = dataManager.GetAudioSettingsData();

        if (settingsPanel == null)
        {
            settingsPanel = gameObject.transform.Find("Settings_Panel").gameObject;
        }
        if (ballVolumeSlider == null)
        {
            ballVolumeSlider = settingsPanel.transform.Find("VolumeSlider_Balls").GetComponent<Slider>();
            ballVolumeSlider.onValueChanged.AddListener(AdjustBallVolume);
            ballVolumeSlider.value = audioData.ballVolume;
        }

        if (crashVolumeSlider == null)
        {
            crashVolumeSlider = settingsPanel.transform.Find("VolumeSlider_Crash").GetComponent<Slider>();
            crashVolumeSlider.onValueChanged.AddListener(AdjustCrashVolume);
            crashVolumeSlider.value = audioData.crashVolume;
        }

        if (notificationVolumeSlider == null)
        {
            notificationVolumeSlider = settingsPanel.transform.Find("VolumeSlider_Notification").GetComponent<Slider>();
            notificationVolumeSlider.onValueChanged.AddListener(AdjustNotificationVolume);
            notificationVolumeSlider.value = audioData.notificationVolume;
        }

        if (backButton == null)
        {
            backButton = settingsPanel.transform.Find("SettingsBackButton").GetComponent<Button>();
            backButton.onClick.AddListener(ToggleSettingsMenu);
        }

        settingsChanged = false;
        settingsPanel.SetActive(false);
    }

    public void ToggleSettingsMenu()
    {
        if (settingsChanged == true)
        {
            dataManager.SetAudioSettingsData(audioData);
        }

        settingsChanged = false;
        settingsPanel.SetActive(!settingsPanel.activeInHierarchy);
    }

    private void AdjustNotificationVolume(float arg0)
    {
        settingsChanged = true;
        audioData.notificationVolume = arg0;
    }

    private void AdjustCrashVolume(float arg0)
    {
        settingsChanged = true;
        audioData.crashVolume = arg0;
    }

    private void AdjustBallVolume(float arg0)
    {
        settingsChanged = true;
        audioData.ballVolume = arg0;
    }
}
