﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// DEPRICATED - Refer BackgroundScrolling.cs
/// </summary>
public class BackgroundController : MonoBehaviour {

    public Transform roadTile;
    public float roadMoveSpeed;

    List<Transform> roadTiles;
    public int roadTilesToPool;
    float roadTileHeight;
    Vector3 tileHoldingPos = Vector3.right * 100;
    public Transform spawnPosZero;

    bool isPlaying = true;
    bool spawnNext = false;

    public bool IsPlaying
    {
        set { isPlaying = value; }
    }

	void Start () {

        roadTileHeight = roadTile.gameObject.GetComponent<SpriteRenderer>().size.y;
        roadTiles = new List<Transform>();
        for (int i = 0; i < roadTilesToPool; i++)
        {
            Transform t = (Transform)Instantiate(roadTile);
            t.position = tileHoldingPos;
            t.gameObject.SetActive(false);
            roadTiles.Add(t);

            if(i < 2)
            {
                Vector3 spnPos = spawnPosZero.position;
                spnPos.y += roadTileHeight * i;
                SpawnRoadTile(spnPos);
            }
        }
        
	}
	

	void Update () {
		
        if(isPlaying == false)
        {
            return;
        }
        for (int i = 0; i < roadTiles.Count; i++)
        {
            if(roadTiles[i].gameObject.activeInHierarchy)
            {
                roadTiles[i].Translate(-Vector2.up * roadMoveSpeed * Time.deltaTime);
                if(spawnNext == true && roadTiles[i].position.y <= spawnPosZero.position.y)
                {
                    spawnNext = false;
                    SpawnRoadTile(spawnPosZero.position + (Vector3.up * roadTileHeight));
                }
                if(roadTiles[i].position.y <= spawnPosZero.position.y - roadTileHeight )
                {
                    roadTiles[i].gameObject.SetActive(false);
                    roadTiles[i].position = tileHoldingPos;
                    spawnNext = true;
                }
            }
        }

	}

    void SpawnRoadTile(Vector3 pos)
    {
        for (int i = 0; i < roadTiles.Count; i++)
        {
            if(!roadTiles[i].gameObject.activeInHierarchy)
            {
                roadTiles[i].position = pos;
                roadTiles[i].rotation = Quaternion.identity;
                roadTiles[i].gameObject.SetActive(true);
                return;
            }
        }
    }
}
