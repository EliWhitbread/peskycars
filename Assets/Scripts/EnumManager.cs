﻿
/// <summary>
/// Holder for all public Enums
/// </summary>

public enum PickupType { AMMO, DAMAGE_BOOST, COIN }

public enum EntityTypes { CAR, TRUCK, BIKE, PICKUP, NULL }

public enum GameState { PLAYER_ALIVE, PLAYER_DEAD, LEVEL_SETUP_NEW, LEVEL_SETUP_RESPAWN }