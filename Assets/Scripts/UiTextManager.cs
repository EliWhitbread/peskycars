﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Random = UnityEngine.Random;


struct SMNotificationData
{
    public int triggerID;
    public int displayScore;
    public float barFillVal;
}

public class UiTextManager : MonoBehaviour {

    public GameObject menuCanvas, gameOverCanvas, settingsPanel;
    public TextMeshProUGUI levelScoreText, scoreValueText, highScoreText, scoreMultiplierText, ammoText;
    private Image scoreMultiplierBar;
    [SerializeField] private Sprite speakerImage, speakerMuteImage, settingsImage, settingsBackImage;
    private Animator ammoTextAnim, scoreMultiplierTextAnim;
    private int ammoTriggerID, scoreMultiplierUpID, scoreMultiplierDownID;
    private Queue<SMNotificationData> scoreMultiplierNotificationQueue;
    private bool scoreMultiplierNotifyIsRunning = false;
    private Button moveButton_Left, moveButton_Right, audioMuteButton;
    private PlayerController player;

    private void Awake()
    {
        scoreMultiplierBar = gameObject.transform.Find("ScoreMultiplyIndicator").GetComponent<Image>();
        scoreMultiplierText = gameObject.transform.Find("ScoreMultiplyText").GetComponent<TextMeshProUGUI>();
        ammoText = gameObject.transform.Find("AmmoText").GetComponent<TextMeshProUGUI>();
        ammoTextAnim = ammoText.gameObject.GetComponent<Animator>();
        ammoTriggerID = Animator.StringToHash("CollectedAmmo");
        scoreMultiplierTextAnim = scoreMultiplierText.gameObject.GetComponent<Animator>();
        scoreMultiplierUpID = Animator.StringToHash("SM_Up");
        scoreMultiplierDownID = Animator.StringToHash("SM_Down");
        scoreMultiplierNotificationQueue = new Queue<SMNotificationData>();
        audioMuteButton = GameObject.FindGameObjectWithTag("AudioMuteButton").GetComponent<Button>();
        audioMuteButton.onClick.AddListener(ToggleInGameVolumeMute);

        //controls
        moveButton_Left = gameObject.transform.Find("MoveButton_Left").GetComponent<Button>();
        moveButton_Right = gameObject.transform.Find("MoveButton_Right").GetComponent<Button>();
        moveButton_Left.onClick.AddListener(MovePlayerLeft);
        moveButton_Right.onClick.AddListener(MovePlayerRight);
    }

    void MovePlayerLeft()
    {
        if (player == null)
        {
            player = GameManager.curGameManager.player.GetComponent<PlayerController>();
        }

        if (player)
        {
            player.MovePlayerLeftRight(false);
        }
        
    }

    private void Start()
    {
        Vector3 _point;
        RectTransform _rt = GameObject.FindGameObjectWithTag("PlayerAmmoIcon").GetComponent<RectTransform>();
        Vector2 _tempPos = _rt.position;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(_rt, _tempPos, Camera.main, out _point);
        
        GameManager.curGameManager.AmmoDisplayUIPosition = _point;
        audioMuteButton.GetComponent<Image>().sprite = AudioManager.audioManager.IsMuteInGame ? speakerMuteImage : speakerImage;
    }

    void MovePlayerRight()
    {
        if (player == null)
        {
            player = GameManager.curGameManager.player.GetComponent<PlayerController>();
        }

        if (player)
        {
            player.MovePlayerLeftRight(true);
        }
    }

    public void UpdateScoreText(int val)
    {
        scoreValueText.text = val.ToString();
    }

    public void UpdateHighScoreText(int val)
    {
        highScoreText.text = String.Format("HIGH: {0}", val.ToString());
    }

    public void UpdateAmmoText(int val)
    {
        ammoText.text = val.ToString();
    }

    public void PlayAmmoCollectedEffects()
    {
        if (ammoTextAnim == null)
        {
            ammoTextAnim = ammoText.gameObject.GetComponent<Animator>();
            ammoTriggerID = Animator.StringToHash("CollectedAmmo");
        }
        ammoTextAnim.SetTrigger(ammoTriggerID);
    }

    public void ResetAnimTriggers()
    {
        if (ammoTextAnim == null)
        {
            Debug.Log("assign AmmoTextAnim");
            ammoTextAnim = ammoText.gameObject.GetComponent<Animator>();
            ammoTriggerID = Animator.StringToHash("CollectedAmmo");
        }

        if (scoreMultiplierTextAnim == null)
        {
            Debug.Log("assign scoreMultiplierAnim");
            scoreMultiplierTextAnim = scoreMultiplierText.gameObject.GetComponent<Animator>();
            scoreMultiplierUpID = Animator.StringToHash("SM_Up");
            scoreMultiplierDownID = Animator.StringToHash("SM_Down");
        }

        ammoTextAnim.ResetTrigger(ammoTriggerID);
        scoreMultiplierTextAnim.ResetTrigger(scoreMultiplierUpID);
        scoreMultiplierTextAnim.ResetTrigger(scoreMultiplierDownID);
    }

    //leave as public for clearing/setting without effect
    public void UpdateScoreMultiplier(int scoreVal, float barVal)
    {
        if (scoreMultiplierBar == null)
        {
            scoreMultiplierBar = gameObject.transform.Find("ScoreMultiplyIndicator").GetComponent<Image>();
        }

        if (scoreMultiplierText == null)
        {
            scoreMultiplierText = gameObject.transform.Find("ScoreMultiplyText").GetComponent<TextMeshProUGUI>();
        }

        scoreMultiplierBar.fillAmount = Mathf.Clamp(barVal, 0.0f, 1.0f);
        scoreMultiplierText.text = String.Format("X{0}", scoreVal.ToString());
    }

    public void AddScoreMultiplierNotification(bool isUp, int multiplierVal, float fillVal)
    {
        SMNotificationData data = new SMNotificationData();
        data.triggerID = isUp ? scoreMultiplierUpID : scoreMultiplierDownID;
        data.displayScore = multiplierVal;
        data.barFillVal = fillVal;
        scoreMultiplierNotificationQueue.Enqueue(data);

        if (!scoreMultiplierNotifyIsRunning)
        {
            StartCoroutine(ScoreMultiplierQueue());
        }
    }


    IEnumerator ScoreMultiplierQueue()
    {
        scoreMultiplierNotifyIsRunning = true;

        while (scoreMultiplierNotificationQueue.Count > 0)
        {
            SMNotificationData data = scoreMultiplierNotificationQueue.Dequeue();
            PlayScoreMultiplierEffect(data.triggerID);

            UpdateScoreMultiplier(data.displayScore, data.barFillVal);
            if (data.triggerID == scoreMultiplierDownID)
            {
                AudioManager.audioManager.PlayAudioOneShot(AudioClipSelection.CarHorn, Random.Range(0.95f, 1.05f));
            }

            yield return new WaitForSeconds(0.5f);
        }

        scoreMultiplierNotifyIsRunning = false;
    }

    void PlayScoreMultiplierEffect(int Id)
    {
        if (scoreMultiplierTextAnim == null)
        {
            scoreMultiplierTextAnim = scoreMultiplierText.gameObject.GetComponent<Animator>();
            scoreMultiplierUpID = Animator.StringToHash("SM_Up");
            scoreMultiplierDownID = Animator.StringToHash("SM_Down");
        }

        //scoreMultiplierTextAnim.SetTrigger(isUp? scoreMultiplierUpID : scoreMultiplierDownID);
        scoreMultiplierTextAnim.SetTrigger(Id);
    }

    public void EnableGameOverCanvas(bool _enable, int _levelScore = 0)
    {
        menuCanvas.SetActive(_enable);
        gameOverCanvas.SetActive(_enable);
        if(_enable == true)
        {
            levelScoreText.text = _levelScore.ToString();
        }
    }

    void ToggleInGameVolumeMute()
    {
        bool isMute = !AudioManager.audioManager.IsMuteInGame;
        if (isMute == true)
        {
            audioMuteButton.GetComponent<Image>().sprite = speakerMuteImage;
        }
        else
        {
            audioMuteButton.GetComponent<Image>().sprite = speakerImage;
        }

        AudioManager.audioManager.IsMuteInGame = isMute;
    }
}
