using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[Serializable]
public struct AudioSettingsData
{
    public bool isMuted;
    public float ballVolume;
    public float crashVolume;
    public float notificationVolume;
}

[Serializable]
public struct GameData
{
    public int highScore;
}

public class PersistentDataManager : MonoBehaviour
{
    private int highScore = 0;
    //private string LocalHighScoreKey = "LocalHighScore";
    private string settingsDataPath, gameDataPath;

    void Awake()
    {
        GetSettingsDataPath();
        GetGameDataPath();
    }

    void GetSettingsDataPath()
    {
        settingsDataPath = Path.Combine(Application.persistentDataPath, "PeskyCarsSettingsData.txt");
        if (!File.Exists(settingsDataPath))
        {
            AudioSettingsData tempData = new AudioSettingsData();
            tempData.isMuted = false;
            tempData.ballVolume = 0.8f;
            tempData.crashVolume = 0.8f;
            tempData.notificationVolume = 0.8f;

            string jsonString = JsonUtility.ToJson(tempData);
            using(StreamWriter streamWriter = File.CreateText(settingsDataPath))
            {
                streamWriter.Write(jsonString);
            }
        }
    }

    void GetGameDataPath()
    {
        gameDataPath = Path.Combine(Application.persistentDataPath, "AreYouCheating.txt");
        if (!File.Exists(gameDataPath))
        {
            GameData tempData = new GameData();
            tempData.highScore = 0;

            string jsonString = JsonUtility.ToJson(tempData);
            using(StreamWriter streamWriter = File.CreateText(gameDataPath))
            {
                streamWriter.Write(jsonString);
            }
        }
    }

    public void SaveGameData(GameData data)
    {
        if (gameDataPath == null)
        {
            GetGameDataPath();
        }

        if (data.highScore > highScore)
        {
            string jsonString = JsonUtility.ToJson(data);
            using(StreamWriter streamWriter = File.CreateText(gameDataPath))
            {
                streamWriter.Write(jsonString);
            }
        }
    }

    public GameData GetGameData()
    {
        if (gameDataPath == null)
        {
            GetGameDataPath();
        }

        using (StreamReader streamReader = File.OpenText(gameDataPath))
        {
            string jsonString = streamReader.ReadToEnd();
            return JsonUtility.FromJson<GameData>(jsonString);
        }
    }

    public void SetAudioSettingsData(AudioSettingsData data)
    {
        if (settingsDataPath == null)
        {
            GetSettingsDataPath();
        }

        string jsonString = JsonUtility.ToJson(data);
        using(StreamWriter streamWriter = File.CreateText(settingsDataPath))
        {
            streamWriter.Write(jsonString);
        }
    }

    public AudioSettingsData GetAudioSettingsData()
    {
        if (settingsDataPath == null)
        {
            GetSettingsDataPath();
        }

        using (StreamReader streamReader = File.OpenText(settingsDataPath))
        {
            string jsonString = streamReader.ReadToEnd();
            return JsonUtility.FromJson<AudioSettingsData>(jsonString);
        }
    }
}
