﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class BarrierGenerator : MonoBehaviour {

    EdgeCollider2D edgeCollider;
    BoxCollider2D floorCollider;
    float screenWidth = 0.0f, screenHeight = 0.0f, edgeColliderPadding = 0.10f;
    Camera mCam;

    // Use this for initialization
	void Start () {

        edgeCollider = gameObject.GetComponent<EdgeCollider2D>();
        floorCollider = gameObject.transform.GetChild(0).GetComponent<BoxCollider2D>();
        screenHeight = Screen.height;
        screenWidth = Screen.width;
        edgeColliderPadding = edgeCollider.edgeRadius;
        mCam = Camera.main;

        SetColliders();
	}
	
	void SetColliders()
    {
        //floor collider
        floorCollider.offset = new Vector2(0.0f, -mCam.orthographicSize - 0.1f);

        //walls and upper collider
        float _height = mCam.orthographicSize;
        float _width = _height * mCam.aspect;
        Vector2[] tempPoints = new Vector2[] { new Vector2(-_width - edgeColliderPadding , -_height - 0.5f),
            new Vector2(-_width - edgeColliderPadding, _height + edgeColliderPadding),
            new Vector2(_width + edgeColliderPadding, _height + edgeColliderPadding),
            new Vector2(_width, -_height - 0.5f) };

        edgeCollider.points = tempPoints;
    }
    
}
