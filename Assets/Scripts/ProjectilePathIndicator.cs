﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePathIndicator : MonoBehaviour {

    [SerializeField] PathIndicatorElement pathIndicatorElement;
    public float pathDistance, elementSpacing;
    public int maxIndicators;
    public Transform pathOrigin;
    public LayerMask layerMask;

    Vector2 rayHitPos = Vector2.zero;
    float rayDist = 0.0f;
    bool pathShowing = false;

    List<PathIndicatorElement> pathElements;
    List<Vector2> pathPoints;

    bool showPath = false;
    private bool bAmmoAvailable = true;

    public bool ShowPath
    {
        set { showPath = value; }
    }

    public bool SetAmmoAvailable
    {
        set => bAmmoAvailable = value;
    }

    private void Awake()
    {
        if (pathIndicatorElement == null)
        {
            pathIndicatorElement = Instantiate(Resources.Load("PathIndicator", typeof(PathIndicatorElement))) as PathIndicatorElement;
            pathIndicatorElement.gameObject.transform.position = Vector3.right * 100;
        }

        pathElements = new List<PathIndicatorElement>(15);
        pathPoints = new List<Vector2>(4);


        for (int i = 0; i < 15; i++)
        {
            PathIndicatorElement _t = (PathIndicatorElement)Instantiate(pathIndicatorElement);
            _t.gameObject.SetActive(false);
            pathElements.Add(_t);
        }
    }

    public void ShowProjectilePath(bool show)
    {
        if(show == showPath)
        {
            return;
        }

        showPath = show;
        if(show == false)
        {
            for (int i = 0; i < pathElements.Count; i++)
            {
                pathElements[i].gameObject.SetActive(false);
            }
        }
        if(showPath == false)
        {
            pathPoints.Clear();
            return;
        }
    }

    public void AddPathPoint(Vector2 point)
    {
        pathPoints.Add(point);
    }

    private void Update()
    {
        if(showPath == false)
        {
            return;
        }

        RaycastHit2D hit = Physics2D.Raycast(pathOrigin.position, pathOrigin.up, 50.0f, ~layerMask);
        rayHitPos = hit.point;
        rayDist = hit.distance;

        if(pathShowing == true)
        {
            return;
        }

        StartCoroutine(DrawPath());

        //for (int i = 0; i < pathElements.Count; i++)
        //{
        //    pathElements[i].SetActive(false);
        //}

        //RaycastHit2D hit = Physics2D.Raycast(pathOrigin.position, pathOrigin.up, 50.0f, ~layerMask);
        //Vector2 _pos = hit.point;
        //Vector2 _dir = _pos - (Vector2)pathOrigin.position;
        //_dir.Normalize();

        //float _dist = hit.distance / elementSpacing;
        //pathDistance = _dist;
        //int _indicators = (int)_dist;


        //for (int i = 0; i < pathElements.Count; i++)
        //{
        //    Vector2 _elementPos = (Vector2)pathOrigin.position + (_dir * (elementSpacing * i));

        //    if (Vector2.Distance(_elementPos, (Vector2)pathOrigin.position) < hit.distance)
        //    {
        //        pathElements[i].transform.position = _elementPos;
        //        pathElements[i].SetActive(true);
        //    }

        //}
    }

    IEnumerator DrawPath()
    {
        pathShowing = true;

        while(showPath == true)
        {
            Vector2 _dir = rayHitPos - (Vector2)pathOrigin.position;
            _dir.Normalize();

            float _dist = rayDist / elementSpacing;
            int _indicators = (int)_dist;

            for (int i = 0; i < pathElements.Count; i++)
            {
                Vector2 _elementPos = (Vector2)pathOrigin.position + (_dir * (elementSpacing * i));

                if (Vector2.Distance(_elementPos, (Vector2)pathOrigin.position) < rayDist)
                {
                    //pathElements[i].transform.position = _elementPos;
                    pathElements[i].UpdateElementPosition(_elementPos);
                    if (!pathElements[i].gameObject.activeInHierarchy)
                    {
                        pathElements[i].gameObject.SetActive(true);
                    }
                    pathElements[i].UpdateCanShoot(bAmmoAvailable);
                    
                }
                else
                {
                    pathElements[i].gameObject.SetActive(false);
                }

            }
            yield return null;
        }
        

        for (int i = 0; i < pathElements.Count; i++)
        {
            pathElements[i].gameObject.SetActive(false);
        }
        pathShowing = false;
    }
    
}
