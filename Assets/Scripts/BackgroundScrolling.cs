﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScrolling : MonoBehaviour {
    
    Material mat;
    [SerializeField]float ySpeed;
    Vector2 matOffset = Vector2.zero;

    private SpriteRenderer roadRenderer;

    public float YSpeed
    {
        set { ySpeed = value; }
    }

    private void Awake()
    {
        //mat = gameObject.GetComponent<MeshRenderer>().material;
        roadRenderer = gameObject.GetComponent<SpriteRenderer>();
        mat = roadRenderer.material;
    }

    private void Start()
    {
        ResizeBackground();
    }

    void ResizeBackground()
    {
        Camera _mCam = Camera.main;
        float _height = _mCam.orthographicSize;
        float _width = _height * _mCam.aspect;

        roadRenderer.transform.position = Vector3.zero;
        roadRenderer.size = new Vector2(_width * 2, _height * 2.1f); //a little padding in Y
    }

    private void Update()
    {
        matOffset.y += ySpeed * Time.deltaTime;

        mat.mainTextureOffset = matOffset;
    }
}
