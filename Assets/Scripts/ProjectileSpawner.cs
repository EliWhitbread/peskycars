﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ProjectileState
{
    Alive,
    Dead,
    Boost
}

public class ProjectileSpawner : MonoBehaviour {

    public static ProjectileSpawner curProjectileSpawner { get; private set; }

    public GameObject projectile;
    public float force, spawnDelay;
    public Transform spawnPos;

    public List<GameObject> projectiles;

    private bool bAmmoAvailable;

    float _time = 0;
    [SerializeField]bool canShoot = false, poolReady = true;
    [SerializeField]int availableProjectileCount = 0, activeProjectiles = 0, waitingProjectiles = 0, projectilesToFire = 0;

    [SerializeField]PlayerController playerController;
    
    public PlayerController PlayerController
    {
        set { playerController = value; }
    }

    /// <summary>
    /// Increment the number of "active" projectiles - required to stop re-spawning projectiles that become inactive while player still shooting
    /// </summary>
    public int ActiveProjectiles
    {
        get { return activeProjectiles; }
        set { activeProjectiles += value; }
    }

    /// <summary>
    /// the number of disabled projectiles waiting to be spawned - used along with "ActiveProjectiles"
    /// </summary>
    public int WaitingProjectiles
    {
        set { waitingProjectiles += value; }
    }

    public int CurrentProjectileCount()
    {
        return projectiles.Count;
    }

    public bool CanShoot
    {
        set { canShoot = value; }
    }

    public bool GetAmmoAvailable()
    {
        return bAmmoAvailable;
    }

    private void Awake()
    {
        if(curProjectileSpawner != null && curProjectileSpawner != this)
        {
            Destroy(this);
        }
        else
        {
            curProjectileSpawner = this;
        }
        projectiles = new List<GameObject>(10);
    }

    //void Start () {

    //    projectiles = new List<GameObject>(10);
    //    //playerController.SetAvailableAmmo(0);
    //    //AddProjectileToPool(1);
    //    //availableProjectileCount = projectiles.Count;
    //    //waitingProjectiles = availableProjectileCount;

    //}

    public void InitProjectilePool()
    {
        playerController.SetAvailableAmmo(0);
        AddProjectileToPool(1);
        availableProjectileCount = projectiles.Count;
        waitingProjectiles = availableProjectileCount;
    }
	
    public void Fire()
    {
        if(canShoot == false)
        {
            canShoot = true;
            availableProjectileCount = projectiles.Count;
            projectilesToFire = 0;
            for (int i = 0; i < projectiles.Count; i++)
            {
                if(!projectiles[i].activeInHierarchy)
                {
                    projectilesToFire++;
                }
            }
            activeProjectiles = 0;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) //editor testing
        {
            if (canShoot == false && waitingProjectiles == availableProjectileCount)
            {
                canShoot = true;
                availableProjectileCount = projectiles.Count;
                activeProjectiles = 0;
                //poolReady = false;
            }
            else
            {
                StartCoroutine(ReturnProjectiles());
                canShoot = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space)) //editor testing
        {
            AddProjectileToPool(1);
        }


        if (canShoot == false) //stop spawning projectiles if false
        {
            return;
        }

        _time += Time.deltaTime;

        if (_time >= spawnDelay)
        {
            _time = 0;

            //trying something new
            if (projectilesToFire > 0)
            {
                projectilesToFire--;
                SpawnProjectile(spawnPos);
                if (projectilesToFire <= 0)
                {
                    canShoot = false;
                }
            }


            //if(activeProjectiles < availableProjectileCount)
            //{
            //    SpawnProjectile(spawnPos);
            //    activeProjectiles++;
            //}
            //else
            //{
            //    canShoot = false;
            //}


        }



    }

    /// <summary>
    /// Spawn a new Projectile for the projectile pool
    /// </summary>
    /// <param name="spnPos"></param>
    void SpawnProjectile(Transform spnPos )
    {
        if(!GameManager.curGameManager.player.activeInHierarchy)
        {
            return;
        }
        for (int x = 0; x < projectiles.Count; x++)
        {
            if (!projectiles[x].activeInHierarchy)
            {
                projectiles[x].transform.position = spnPos.position;
                projectiles[x].transform.rotation = spnPos.rotation;
                ProjectileMover pMover = projectiles[x].GetComponent<ProjectileMover>();
                pMover.MoveForce = force;
                projectiles[x].SetActive(true);
                pMover.Fire();
                playerController.AvailableAmmo = -1;
                waitingProjectiles--;
                bAmmoAvailable = x < projectiles.Count;
                return;
            }
        }
    }

    /// <summary>
    /// add n new projectiles to the projectile pool 
    /// </summary>
    /// <param name="amt"></param>
    internal void AddProjectileToPool(int amt)
    {
        for (int i = 0; i < amt; i++)
        {
            GameObject obj = Instantiate(projectile) as GameObject;
            obj.transform.position = spawnPos.position;
            obj.transform.rotation = spawnPos.rotation;
            ProjectileMover pMover = obj.GetComponent<ProjectileMover>();
            if (pMover == null)
            {
                Debug.LogError("No ProjectileMover component found on Projectile");
            }
            pMover.InitProjectile();
            obj.SetActive(false);
            projectiles.Add(obj);
            //playerController.AvailableAmmo = 1;
            //if (GameManager.curGameManager.player != null)
            //{
            //    GameManager.curGameManager.playerController.UpdateAmmoText(waitingProjectiles);
            //}

        }
        if(canShoot == true)
        {
            availableProjectileCount = projectiles.Count;
            waitingProjectiles = availableProjectileCount;
        }
        
    }

    /// <summary>
    /// return all active projectiles to the player's position
    /// </summary>
    public void ReturnAllProjectiles()
    {
        StartCoroutine(ReturnProjectiles());
        canShoot = false;
    }

    /// <summary>
    /// clear all projectiles from the level
    /// </summary>
    public void ClearAllProjectiles(bool clearPool = false)
    {
        canShoot = false;
        if(clearPool == true)
        {
            foreach (GameObject p in projectiles)
            {
                Destroy(p);
            }
            projectiles.Clear();
            playerController.SetAvailableAmmo(0);
            InitProjectilePool();
            //AddProjectileToPool(1);
            return;
        }
        for (int i = 0; i < projectiles.Count; i++)
        {
            projectiles[i].SetActive(false);
        }
    }

    /// <summary>
    /// Set all active projectiles to "Dead" - forcing them to return to the player's position and deactivate
    /// </summary>
    /// <returns></returns>
    ProjectileMover _pm;

    IEnumerator ReturnProjectiles()
    {
        for (int i = 0; i < projectiles.Count; i++)
        {
            yield return new WaitForSeconds(Random.Range(0.01f, 0.2f)); //for visual effect only - stop all projectiles returning together.

            if(projectiles[i].activeInHierarchy)
            {
                _pm = projectiles[i].GetComponent<ProjectileMover>();
                _pm.ProjectileState = ProjectileState.Dead;
            }
        }
    }

    public void ProjectileReturnedToPool()
    {
        playerController.AvailableAmmo = 1;
    }
}
