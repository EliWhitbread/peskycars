using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private Slider ballVolumeSlider, crashVolumeSlider, notificationVolumeSlider;
    private GameObject settingsMenu;
    private Button settingsButton;
    [SerializeField] private Sprite settingsButtonImage, settingsBackButtonImage;
    private AudioSettingsData audioData;
    private bool settingsChanged;

    void OnEnable()
    {
        if (settingsMenu == null)
        {
            settingsMenu = gameObject.transform.Find("Settings_Panel").gameObject;
        }
        if (ballVolumeSlider == null)
        {
            ballVolumeSlider = settingsMenu.transform.Find("VolumeSlider_Balls").GetComponent<Slider>();
            ballVolumeSlider.onValueChanged.AddListener(AdjustBallVolume);
        }

        if (crashVolumeSlider == null)
        {
            crashVolumeSlider = settingsMenu.transform.Find("VolumeSlider_Crash").GetComponent<Slider>();
            crashVolumeSlider.onValueChanged.AddListener(AdjustCrashVolume);
        }

        if (notificationVolumeSlider == null)
        {
            notificationVolumeSlider = settingsMenu.transform.Find("VolumeSlider_Notification").GetComponent<Slider>();
            notificationVolumeSlider.onValueChanged.AddListener(AdjustNotificationVolume);
        }

        if (settingsButton == null)
        {
            settingsButton = gameObject.transform.Find("SettingsButton").GetComponent<Button>();
            settingsButton.onClick.AddListener(ToggleSettingsMenu);
        }

        settingsChanged = false;
       // audioData = AudioManager.audioManager.AudioVolumeData;
        SetSettingsMenuVisibility(false);
    }

    void ToggleSettingsMenu()
    {
        SetSettingsMenuVisibility(!settingsMenu.activeInHierarchy);
    }

    public void SetSettingsMenuVisibility(bool isVisible)
    {
        if (settingsMenu == null)
        {
            settingsMenu = gameObject.transform.Find("Settings_Panel").gameObject;
        }

        if (isVisible == true)
        {
            audioData = AudioManager.audioManager.AudioVolumeData;
            ballVolumeSlider.value = audioData.ballVolume;
            crashVolumeSlider.value = audioData.crashVolume;
            notificationVolumeSlider.value = audioData.notificationVolume;
        }
        else if (settingsChanged == true)
        {
            GameManager.curGameManager.SaveSettingsData(audioData);
            AudioManager.audioManager.RefreshVolumeSettings();
        }

        settingsMenu.SetActive(isVisible);
        settingsButton.GetComponent<Image>().sprite = isVisible ? settingsBackButtonImage : settingsButtonImage;
        settingsChanged = false;
    }

    void AdjustBallVolume(float val)
    {
        settingsChanged = true;
        if (val < 0.01f)
        {
            val = 0.0f;
        }
        audioData.ballVolume = val;
    }

    void AdjustCrashVolume(float val)
    {
        settingsChanged = true;
        if (val < 0.01f)
        {
            val = 0.0f;
        }
        audioData.crashVolume = val;
    }

    void AdjustNotificationVolume(float val)
    {
        settingsChanged = true;
        if (val < 0.01f)
        {
            val = 0.0f;
        }
        audioData.notificationVolume = val;
    }

}
