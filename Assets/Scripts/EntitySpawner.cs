﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EntitySpawner : MonoBehaviour {

    public Entity car;
    public Pickup pickUpPrefab;
    List<Entity> cars;
    public int initialPoolSize, initialLevelEntityCount, initialRowSpawnCount = 10;
    public Sprite[] sprites;
    [SerializeField] Vector3[] spawnPositions;
    //public List<EntitySpawnInfo> entitySpawnData;
    int lastSpawnPos = -1, maxEntitiesPerRow = 3, maxPickupsActive = 4;
    float entitySpeed, entityOffScreenPosY;
    [SerializeField] private LayerMask entityCheckOverlapLayerMask;
    int minEntityHP = 1, maxEntityHP = 3;
    float entityHeightRef = 0.0f, entitySpawnY;

    //spawn position check
    public bool[] availableSpawnPositions = new bool[6]; //6 = road lanes //Public for testing!!
    LevelSpawnData spawnData;
    private Vector2 spawnCheckOverlapSize = new Vector2(0.6f, 1.1f);

    //destroy event
    public delegate void DestroyObjectRequest();
    public static event DestroyObjectRequest OnDestroyRequest;

    //speed update
    public delegate void EntitySpeedUpdate(float val);
    public static event EntitySpeedUpdate OnEntitySpeedUpdate;
    
    //initial level
    private bool bIsInitialLevel = false;

    public int MaxEntitiesPerRow
    {
        get { return maxEntitiesPerRow; }
        set { maxEntitiesPerRow = value; }
    }

    /// <summary>
    /// Set the moveSpeed of Entities
    /// </summary>
    public float EntitySpeed
    {
        set { entitySpeed = value; UpdatAllEntitySpeed(entitySpeed); }
    }

    private void Awake()
    {
        GameObject.DontDestroyOnLoad(this);

        UpdateSpawnPositions();
    }

    private void Start()
    {
        spawnData = GetComponent<LevelSpawnData>();
        cars = new List<Entity>();
        //entitySpawnData = new List<EntitySpawnInfo>(240);
        AddEntityToPool(initialPoolSize);
        //SpawnInitialLevel();
        for (int i = 0; i < availableSpawnPositions.Length; i++)
        {
            availableSpawnPositions[i] = false;
        }

       // spawnData.GenerateSpawnPositionData(spawnPositions);
        spawnData.InitSpawnData(availableSpawnPositions.Length);
        StartCoroutine(GetEntityBounds());
    }

    IEnumerator GetEntityBounds()
    {
        yield return new WaitUntil(() => cars.Count > 0);

        Vector3 entityBounds = cars[0].gameObject.GetComponent<Collider2D>().bounds.size;
        entityHeightRef = entityBounds.y;
        GameManager.curGameManager.EntityHeight = entityHeightRef;
        for (int i = 0; i < spawnPositions.Length; i++)
        {
            Vector3 _pos = spawnPositions[i];
            _pos.y += entityHeightRef / 2;
            spawnPositions[i] = _pos;
        }

        GameObject.FindGameObjectWithTag("EntitySpawnCheck").GetComponent<EntityRowSpawnCheck>().RepositionRowSpawnChecker(spawnPositions[0].y, entityHeightRef);

        entitySpawnY = spawnPositions[0].y;
    }

    void UpdateSpawnPositions()
    {
        Camera _mCam = Camera.main;
        float _scrHeight = _mCam.orthographicSize;
        float _scrnWidth = (_scrHeight * _mCam.aspect) * 2;

        entityOffScreenPosY = -_scrHeight;

        float _spacing = _scrnWidth / 12; //based on 6 lane road background image
        if (spawnPositions.Length != 6)
        {
            spawnPositions = new Vector3[6];

        }

        float _startX = _scrnWidth / 2;
        float _spawnYPos = _scrHeight + 0.1f;

        for (int i = 0; i < spawnPositions.Length; i++)
        {
            spawnPositions[i] = new Vector3( -_startX + _spacing + ((_spacing * 2 )* i), _spawnYPos, 0.0f);
        }

        GameObject.FindGameObjectWithTag("EntitySpawnCheck").GetComponent<EntityRowSpawnCheck>().InitSpawnChecker(_spawnYPos, _scrnWidth);
    }

    public void SetMinMaxEntityHP(int minHP, int maxHP)
    {
        minEntityHP = minHP;
        maxEntityHP = maxHP;
    }

    /// <summary>
    /// Initial level propogation - Spawn a random-ish number of Entities at game start
    /// </summary>
    public void SpawnInitialLevel()
    {
        //initialLevelEntityCount = new List<int>();
        //int amt = ProjectileSpawner.curProjectileSpawner.CurrentProjectileCount();
        //if (amt == 0)
        //{
        //    amt = 1;
        //}

        /*List<EntitySpawnInfo> rowSpawnInfo*/;

        //for (int x = 0; x < initialRowSpawnCount; x++)

        bIsInitialLevel = true;

        int endPosY = 5; // (int)spawnPositions[0].y;
        for (int x = 1; x < endPosY; x++)
        {
            List<EntitySpawnInfo> rowSpawnInfo = spawnData.ReturnNextEntityRowSpawnData(maxEntitiesPerRow);

            for (int i = 0; i < rowSpawnInfo.Count; i++)
            {
                if (rowSpawnInfo[i].entityType == EntityTypes.CAR)
                {
                    SpawnEntity(rowSpawnInfo[i].entityType, rowSpawnInfo[i].spnPos, (float)x);
                }
                if (rowSpawnInfo[i].entityType == EntityTypes.PICKUP)
                {
                    SpawnPickup(PickupType.AMMO, new Vector3(spawnPositions[rowSpawnInfo[i].spnPos].x, (float)x, 0.0f));
                }
            }
        }

        GameManager.curGameManager.LevelLoaded = true;
        bIsInitialLevel = false;
        //int amt = ProjectileSpawner.curProjectileSpawner.CurrentProjectileCount();
        //if(amt == 0)
        //{
        //    amt = 1;
        //}

        //for (int x = 0; x < initialLevelEntityCount; x++)
        //{
        //    for (int i = 0; i < spawnPositions.Length; i++)
        //    {
        //        int canSpn = Random.Range(0, 3);
        //        if(canSpn == 0)
        //        {
        //            if (!cars[i].gameObject.activeInHierarchy)
        //            {
        //                //cars[i].transform.position = new Vector3(spawnPositions[i].x, 0.0f + x, spawnPositions[i].z);
        //                //cars[i].transform.rotation = Quaternion.identity;
        //                //Entity _en = cars[i].GetComponent<Entity>();
        //                //int r = Random.Range(amt + x, (amt * 2) + x);
        //                //_en.Health = r;
        //                //_en.CanSpawnNextEntity = false;
        //                //_en.MoveSpeed = entitySpeed;
        //                //cars[i].SetActive(true);

        //                cars[i].transform.position = new Vector3(spawnPositions[i].x, 0.0f + x, spawnPositions[i].z);
        //                cars[i].transform.rotation = Quaternion.identity;
        //                //Entity _en = cars[i].GetComponent<Entity>();
        //                int r = Random.Range(amt + x, (amt * 2) + x);
        //                cars[i].Health = r;
        //                cars[i].CanSpawnNextEntity = false;
        //                cars[i].MoveSpeed = entitySpeed;
        //                cars[i].gameObject.SetActive(true);
        //            }
        //        }
        //    }
        //}
    }

    void SpawnEntity(EntityTypes entityTypes, int xPos, float yPos)
    {
        //int amt = ProjectileSpawner.curProjectileSpawner.CurrentProjectileCount();
        //if (amt == 0)
        //{
        //    amt = 1;
        //}

        int _hp = Random.Range(minEntityHP, maxEntityHP);

        for (int i = 0; i < cars.Count; i++)
        {
            if (!cars[i].gameObject.activeInHierarchy)
            {

                cars[i].transform.position = new Vector3(spawnPositions[xPos].x, yPos, 0.0f);
                cars[i].transform.rotation = Quaternion.identity;
                //Entity _en = cars[i].GetComponent<Entity>();
                //int _h = Random.Range(amt / 6, ((amt / 6) * 2));
                cars[i].Health = _hp;
                cars[i].CanSpawnNextEntity = false;
                cars[i].MoveSpeed = entitySpeed;

                //if (CheckForEntityOverlap(cars[i].gameObject.transform.position) == true)
                //{
                //    cars[i].transform.position = Vector2.right * 20;
                //    cars[i].gameObject.SetActive(false);
                //    return;
                //}

                cars[i].IsInitialLevelSpawn = bIsInitialLevel;
                cars[i].gameObject.SetActive(true);

                if (bIsInitialLevel)
                {
                    GameManager.curGameManager.AddInitialEntityToPool(cars[i]);
                }

                return;
            }
        }

        //no free Entities in pool - spawn new
        Entity obj = Instantiate(car) as Entity;
        int rnd = Random.Range(0, sprites.Length);
        obj.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
        obj.gameObject.SetActive(false);
        cars.Add(obj);

        obj.transform.position = new Vector3(spawnPositions[xPos].x, yPos, 0.0f);
        obj.transform.rotation = Quaternion.identity;
        obj.Health = _hp;
        obj.CanSpawnNextEntity = false;
        obj.MoveSpeed = entitySpeed;
        obj.OffScreenY = entityOffScreenPosY;

        //if (CheckForEntityOverlap(obj.gameObject.transform.position) == true)
        //{
        //    obj.transform.position = Vector2.right * 20;
        //    obj.gameObject.SetActive(false);
        //    return;
        //}

        obj.gameObject.SetActive(true);
        obj.IsInitialLevelSpawn = bIsInitialLevel;

        if (bIsInitialLevel)
        {
            GameManager.curGameManager.AddInitialEntityToPool(obj);
            
        }
        
    }

    /// <summary>
    /// Spawn a new Entity Row
    /// </summary>
    public bool SpawnNewEntity()
    {
        bool spawnSuccess = false;

        Debug.Log(maxEntitiesPerRow.ToString());

        List<EntitySpawnInfo> rowSpawnInfo = spawnData.ReturnNextEntityRowSpawnData(maxEntitiesPerRow);

        //float refYPos = (int)spawnPositions[0].y;

        for (int i = 0; i < rowSpawnInfo.Count; i++)
        {
            if (rowSpawnInfo[i].entityType == EntityTypes.CAR)
            {
                SpawnEntity(rowSpawnInfo[i].entityType, rowSpawnInfo[i].spnPos, entitySpawnY);
                spawnSuccess = true;
            }
            if (rowSpawnInfo[i].entityType == EntityTypes.PICKUP)
            {
                SpawnPickup(PickupType.AMMO, new Vector3(spawnPositions[rowSpawnInfo[i].spnPos].x, entitySpawnY, 0.0f));
                spawnSuccess = true;
            }
        }

        return spawnSuccess;
    }


    /// <summary>
    /// Add a new Entity to the cars pool
    /// </summary>
    /// <param name="amt"></param>
    void AddEntityToPool(int amt)
    {
        for (int i = 0; i < amt; i++)
        {
            //GameObject obj = (GameObject)Instantiate(car);
            //int rnd = Random.Range(0, sprites.Length);
            //obj.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
            //obj.SetActive(false);
            //cars.Add(obj);

            Entity obj = Instantiate(car) as Entity;
            int rnd = Random.Range(0, sprites.Length);
            obj.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
            obj.OffScreenY = entityOffScreenPosY;
            obj.gameObject.SetActive(false);
            cars.Add(obj);
        }
        
    }

    /// <summary>
    /// Update the moveSpeed of all active Entities
    /// </summary>
    /// <param name="speed"></param>
    void UpdatAllEntitySpeed(float speed)
    {
        if (OnEntitySpeedUpdate != null)
        {
            OnEntitySpeedUpdate(speed);
        }

        //for (int i = 0; i < cars.Count; i++)
        //{
        //    if(cars[i].gameObject.activeInHierarchy)
        //    {
        //        //Entity _en = cars[i].GetComponent<Entity>();
        //        cars[i].MoveSpeed = speed;
        //    }
        //}
    }

    /// <summary>
    /// Spawn a new Pickup
    /// </summary>
    /// <param name="pickupType">Pickup Enum Type</param>
    /// <param name="pos">Vector 3 position</param>
    /// <param name="val">Pickup value</param>
    internal void SpawnPickup(PickupType pickupType, Vector3 pos, int val = 1)
    {
        //GameObject _puObj = (GameObject)Instantiate(pickup);
        //Pickup _pu = _puObj.GetComponent<Pickup>();
        //_pu.PickupType = pickupType;
        //_pu.PuValue = val;
        //_pu.MoveSpeed = entitySpeed;
        //_puObj.transform.position = pos;
        ////_puObj.transform.rotation = Quaternion.identity;
        //_puObj.SetActive(true);

        Pickup _pu = Instantiate(pickUpPrefab, pos, Quaternion.identity);
        _pu.PickupType = pickupType;
        _pu.PuValue = val;
        _pu.MoveSpeed = entitySpeed;
        _pu.ProjectileLayer = LayerMask.GetMask("Projectile");
        _pu.gameObject.SetActive(true);
    }
    
    internal void ClearLevel()
    {
        for (int i = 0; i < cars.Count; i++)
        {
            cars[i].gameObject.SetActive(false);
        }

        if (OnDestroyRequest != null)
        {
            OnDestroyRequest();
        }
    }

    internal bool CheckForEntityOverlap(Vector3 _testEntity)
    {

        //Collider2D _col = _testEntity.GetComponent<Collider2D>();

        return Physics2D.OverlapBox(_testEntity, spawnCheckOverlapSize, 0.0f, entityCheckOverlapLayerMask);
        //return !Physics2D.OverlapBox(_testEntity.transform.position, _col.bounds.size * 2.0f, 0.0f, entityCheckOverlapLayerMask);


        //for (int i = 0; i < cars.Count; i++)
        //{
        //    if(cars[i].gameObject.activeInHierarchy && cars[i].gameObject != _testEntity && Mathf.Approximately(cars[i].transform.position.x, _testEntity.transform.position.x ) == true)
        //    {
        //        Collider2D _col2 = cars[i].gameObject.GetComponent<Collider2D>();

        //        if(_col.bounds.Intersects(_col2.bounds))
        //        {
        //            return false;
        //        }
        //    }
        //}

        //return true;
    }

    public float GetRandomSpawnPosX()
    {
        if (spawnPositions.Length == 0 || spawnPositions == null)
        {
            Debug.LogError("No Spawn Positions found - EntitySpawner");
            return 0.6f; //just for safety.
        }

        int _r = Random.Range(0, spawnPositions.Length -1);
        return spawnPositions[Mathf.Max(1, _r)].x;
    }

    public float GetDistBetweenSpawnPoints()
    {
        if (spawnPositions.Length == 0 || spawnPositions == null)
        {
            Debug.LogError("No Spawn Positions found - EntitySpawner");
        }

        return Mathf.Abs(spawnPositions[0].x - spawnPositions[1].x);
    }
}