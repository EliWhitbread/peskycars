﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    [SerializeField]PickupType pickupType;
    [SerializeField]int puValue;
    float moveSpeed = 0.30f, killPosY = -7.0f;
    Transform myTransform;
    private bool isActive;
    LayerMask projectileLayer;
    private CollectedPickupDisplayObject pickupProxy;

    public PickupType PickupType
    {
        get { return pickupType; }
        set { pickupType = value; }
    }

    public LayerMask ProjectileLayer
    {
        set => projectileLayer = value;
    }

    public int PuValue
    {
        get { return puValue; }
        set { puValue = value; }
    }

    public float MoveSpeed
    {
        set { moveSpeed = value; }
    }

    private void OnEnable()
    {
        if(myTransform == null)
        {
            myTransform = this.transform;
        }

        if (pickupProxy == null)
        {
            pickupProxy = Resources.Load<CollectedPickupDisplayObject>("PickupCollectionProxy");
        }

        EntitySpawner.OnDestroyRequest += EntitySpawner_OnDestroyRequest;
        EntitySpawner.OnEntitySpeedUpdate += EntitySpawner_OnEntitySpeedUpdate;

        isActive = false;
        StartCoroutine(SetActiveState());
    }

    private void EntitySpawner_OnEntitySpeedUpdate(float val)
    {
        moveSpeed = val;
    }

    private void OnDisable()
    {
        EntitySpawner.OnDestroyRequest -= EntitySpawner_OnDestroyRequest;
        EntitySpawner.OnEntitySpeedUpdate -= EntitySpawner_OnEntitySpeedUpdate;
    }

    private void EntitySpawner_OnDestroyRequest()
    {
        Destroy(this.gameObject);
    }

    IEnumerator SetActiveState()
    {
        yield return new WaitUntil(() => IsOverlapping() == false);
        isActive = true;
    }

    bool IsOverlapping()
    {
        return Physics2D.OverlapCircle(myTransform.position, 0.23f, projectileLayer);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(!isActive) {return;}

        if(other.CompareTag("Player") || other.CompareTag("Projectile"))
        {
            AudioManager.audioManager.PlayAudioOneShot(AudioClipSelection.BallHit, 1.8f);
            //GameManager.curGameManager.CollectedPickup(pickupType, puValue);
            //gameObject.SetActive(false);
           // Destroy(this.gameObject); //testing non-pooled pickups - memory alloc vs GC performance?
            CollectedPickupDisplayObject _proxy = (CollectedPickupDisplayObject)Instantiate(pickupProxy, myTransform.position, myTransform.rotation);
            _proxy.InitPickupProxy(pickupType, puValue);
            _proxy.gameObject.SetActive(true);
            gameObject.SetActive(false);
            myTransform.position = Vector2.right * 50; //get it outa there
        }
    }

    void Update () {

        myTransform.Translate(-Vector2.up * (moveSpeed * Time.deltaTime));

        if (myTransform.position.y < killPosY)
        {
            //gameObject.SetActive(false);
            //Destroy(this.gameObject); //testing non-pooled pickups - memory alloc vs GC performance?
            myTransform.position = Vector2.right * 50; //get it outa there
        }
	}
}
