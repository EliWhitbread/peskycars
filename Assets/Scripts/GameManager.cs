﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public delegate void ActiveGameState(GameState gameState);
    public static event ActiveGameState CurrentActiveGameState;

    public static GameManager curGameManager { get; private set; }

    public GameObject player;
    private PersistentDataManager pDataManager;
    public PlayerController playerController;
    public UiTextManager dynamicUiText;
    public BackgroundScrolling backgroundScroller;
    [SerializeField] int currentScore = 0, highScore = 0,  curMinEntityValue = 0, curMaxEntityValue = 0;
    EntitySpawner entitySpawner;
    public int minEntityValue, maxEntityValue;
    public float speedUpDelay;
    float speedUpTime = 0.0f, timeSinceLastSpawn = 10.0f;
    [SerializeField] float initPlayerSpeed = 0.10f, initEntitySpeed = 0.30f, playerSpeed = 0.10f, entitySpeed = 0.30f;
    public Vector3 playerSpawnPosition;
    bool levelLoaded = false;
    private bool playerAlive;
    private int scoreMultiplier = 0, scoreMultiplierMax = 10;
    private float scoreMultiplierIncrementTimer = 0.0f, scoreMultiplierIncrementDelay = 60.0f;
    private int entitiesDestroyed = 0; //TODO: increment for level change.
    private Vector2 ammoDisplayUIPosition;
    float entityHeight = 1.0f;
    [SerializeField] bool canSpawnNextRow = true;
    [SerializeField] private int rowsSpawnedThisLevel = 0;

    //initial level
    private bool bIsInitialLevel;
    [SerializeField] private List<Entity> initialLevelEntities;
    private int initLevelEntityCountRef;

    public Vector2 AmmoDisplayUIPosition
    {
        get => ammoDisplayUIPosition;
        set => ammoDisplayUIPosition = value;
    }

    public bool LevelLoaded
    {
        get { return levelLoaded; }
        set { levelLoaded = value; }
    }

    public bool IsInitialLevel
    {
        get => bIsInitialLevel;
        set => bIsInitialLevel = value;
    }

    public float EntityHeight
    {
        set => entityHeight = value;
    }

    public bool CanSpawnNextRow
    {
        set => canSpawnNextRow = value;
    }

    private void Awake()
    {
        if(curGameManager != null && curGameManager != this)
        {
            Destroy(this);
        }
        else
        {
            curGameManager = this;
        }
        UpdatePlayerScore(currentScore);
        dynamicUiText.EnableGameOverCanvas(false);
    }

    private void Start()
    {
        entitySpawner = GameObject.FindGameObjectWithTag("EntitySpawner").GetComponent<EntitySpawner>();
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            
        }

        if (pDataManager == null)
        {
            pDataManager = gameObject.GetComponent<PersistentDataManager>();
        }

        bIsInitialLevel = true; //TODO: maybe switch to player selectable - could slow play after multiple restarts.
        initialLevelEntities = new List<Entity>();
        initLevelEntityCountRef = 0;

        scoreMultiplier = 0;
        highScore = pDataManager.GetGameData().highScore;
        dynamicUiText.UpdateHighScoreText(highScore);
        playerController = player.GetComponent<PlayerController>();
        ProjectileSpawner.curProjectileSpawner.PlayerController = playerController;
        playerSpeed = initPlayerSpeed;
        entitySpeed = initEntitySpeed;
        entitySpawner.EntitySpeed = 0.0f; // entitySpeed; - initial level speed = 0 for tutorial reasons.
        backgroundScroller.YSpeed = playerSpeed;
        curMinEntityValue = minEntityValue;
        curMaxEntityValue = maxEntityValue;
        playerController.LaneSpacing = entitySpawner.GetDistBetweenSpawnPoints();
        LoadLevel();
    }

    private void Update()
    {
        if(levelLoaded == false || !playerAlive || bIsInitialLevel == true)
        {
            return;
        }

        speedUpTime += Time.deltaTime;

       // timeSinceLastSpawn += Time.deltaTime;

        if(canSpawnNextRow) //(timeSinceLastSpawn >= entityHeight / entitySpeed)
        {
            //timeSinceLastSpawn = 0.0f;
            SpawnNextEntity();
            canSpawnNextRow = false;
            
            UpdateMaxEntiriesPerRow(rowsSpawnedThisLevel++);
        }

        if(speedUpTime >= speedUpDelay) //increment the Entity and Background move speed
        {
            speedUpTime = 0.0f;
            
            playerSpeed += 0.01f; //TODO - magic number for testing only
            backgroundScroller.YSpeed = playerSpeed;
            entitySpeed += 0.02f; //TODO - magic number for testing only
            entitySpawner.EntitySpeed = entitySpeed;
        }

        if (scoreMultiplier < scoreMultiplierMax)
        {
            scoreMultiplierIncrementTimer += Time.deltaTime;

            if (scoreMultiplierIncrementTimer >= scoreMultiplierIncrementDelay)
            {
                UpdatePlayerScoreMutiplier(1);
            }
        }

        //TODO: add difficulty increment - fine tune speed update AND EntitySpawner.MaxEntitiesPerRow 
        
    }

    void UpdateMaxEntiriesPerRow(int value) //TODO: update ranges after testing.
    {
        if(value > 11) return;

        switch (value)
        {
            case int n when n <= 5:
                entitySpawner.MaxEntitiesPerRow = 3;
                break;
            case int n when n > 5 && n <= 10:
                entitySpawner.MaxEntitiesPerRow = 4;
                break;
            case int n when n > 10:
                entitySpawner.MaxEntitiesPerRow = 5;
                break;
                break;
            default:
                entitySpawner.MaxEntitiesPerRow = 3;
                break;
        }
    }

    /// <summary>
    /// Initialise the level
    /// </summary>
    /// <param name="isContinue"></param>
    public void LoadLevel(bool isContinue = false)
    {
        ProjectileSpawner.curProjectileSpawner.ClearAllProjectiles(!isContinue);
        clearInitialLevelEntities();

        if (isContinue == false)
        {
            curMinEntityValue = minEntityValue;
            curMaxEntityValue = maxEntityValue;
            playerSpeed = initPlayerSpeed;
            entitySpeed = initEntitySpeed;
            entitySpawner.SpawnInitialLevel();
            bIsInitialLevel = true;
            currentScore = 0;
            rowsSpawnedThisLevel = 0;
           
            UpdatePlayerScore(currentScore);
        }

        UpdateMaxEntiriesPerRow(rowsSpawnedThisLevel);
        entitySpawner.SetMinMaxEntityHP(curMinEntityValue, curMaxEntityValue);

        //int rPosX = Random.Range(0, 2);
        playerSpawnPosition.x = entitySpawner.GetRandomSpawnPosX();
        player.transform.position = playerSpawnPosition;
        player.transform.rotation = Quaternion.identity;
        player.SetActive(true);
        playerAlive = true;
        backgroundScroller.YSpeed = playerSpeed;
        //SpawnNextEntity();
        dynamicUiText.EnableGameOverCanvas(false);
        dynamicUiText.ResetAnimTriggers();
        entitySpawner.EntitySpeed = bIsInitialLevel? 0.0f : entitySpeed;
        scoreMultiplier = 0;
        UpdatePlayerScoreMutiplier(1);
        scoreMultiplierIncrementTimer = 0.0f;

        ProjectileSpawner.curProjectileSpawner.CanShoot = false;
    }
    
    /// <summary>
    /// Update the score value in the UI
    /// </summary>
    /// <param name="val"></param>
    public void UpdatePlayerScore(int val)
    {
        currentScore += (val * Mathf.Max(1,scoreMultiplier));
        dynamicUiText.UpdateScoreText(currentScore);

        //high score
        if (currentScore > highScore)
        {
            highScore = currentScore;
            dynamicUiText.UpdateHighScoreText(highScore);
            GameData data = pDataManager.GetGameData();
            data.highScore = highScore;
           // pDataManager.SetHighScore(highScore);
            pDataManager.SaveGameData(data);
        }

        if (bIsInitialLevel)
        {

        }
    }

    public AudioSettingsData GetSettingsData()
    {
        return pDataManager.GetAudioSettingsData();
    }

    public void SaveSettingsData(AudioSettingsData data)
    {
        pDataManager.SetAudioSettingsData(data);
    }

    public void UpdatePlayerScoreMutiplier(int val)
    {
        int oldScoreMultiplier = scoreMultiplier;

        scoreMultiplier += val;

        scoreMultiplier = Mathf.Clamp(scoreMultiplier, 1, scoreMultiplierMax);

        if (scoreMultiplier != oldScoreMultiplier && val != 0)
        {
            float _IndicatorVal = (float)scoreMultiplier / (float)scoreMultiplierMax;

            if (scoreMultiplier < oldScoreMultiplier)
            {
                //dynamicUiText.PlayScoreMultiplierEffect(false);
                dynamicUiText.AddScoreMultiplierNotification(false, scoreMultiplier, _IndicatorVal);
            }
            else
            {
                //dynamicUiText.PlayScoreMultiplierEffect(true);
                dynamicUiText.AddScoreMultiplierNotification(true, scoreMultiplier, _IndicatorVal);
            }
            
            

            //dynamicUiText.UpdateScoreMultiplier(scoreMultiplier, _IndicatorVal);
        }

        scoreMultiplierIncrementTimer = 0.0f;
    }

    /// <summary>
    /// Spawn a new Entity
    /// </summary>
    public void SpawnNextEntity()
    {
        if (entitySpawner.SpawnNewEntity())
        {
            timeSinceLastSpawn = 0.0f;
            return;
        }

        timeSinceLastSpawn = timeSinceLastSpawn * 0.8f;

    }

    /// <summary>
    /// Spawn a new Pickup
    /// </summary>
    /// <param name="puType">PickUp Enum Type</param>
    /// <param name="pos">vector 3 - position</param>
    public void SpawnPickup(PickupType puType, Vector3 pos)
    {
        entitySpawner.SpawnPickup(puType, pos);
    }

    public void CollectedPickup(PickupType _puType, int _val)
    {
        switch (_puType)
        {
            case PickupType.AMMO:
                ProjectileSpawner.curProjectileSpawner.AddProjectileToPool(_val);
                int amt = ProjectileSpawner.curProjectileSpawner.CurrentProjectileCount();
                dynamicUiText.PlayAmmoCollectedEffects();
                curMinEntityValue = (int)amt / 3;
                if(curMinEntityValue < minEntityValue)
                {
                    curMinEntityValue = minEntityValue; //safety
                }
                curMaxEntityValue = (int)Mathf.Min((amt * 0.9f), curMinEntityValue * 2);
                if (curMaxEntityValue < maxEntityValue)
                {
                    curMaxEntityValue = maxEntityValue;
                }
                entitySpawner.SetMinMaxEntityHP(curMinEntityValue, curMaxEntityValue);
                break;
            case PickupType.DAMAGE_BOOST:
                break;
            case PickupType.COIN:
                break;
            default:
                break;
        }
    }

    public void PlayerDied()
    {
        playerAlive = false;
        backgroundScroller.YSpeed = 0.0f;
        StartCoroutine(ShowGameOver());
    }

    IEnumerator ShowGameOver()
    {
        yield return new WaitForSeconds(1.3f);
        entitySpawner.ClearLevel();
        dynamicUiText.EnableGameOverCanvas(true, currentScore);
    }

    public void UpdateAmmoText(int val)
    {
        dynamicUiText.UpdateAmmoText(val);
    }

    public void PlayerRespawn(int _selection) //set in button action - only 2 options atm, may extend
    {
        LoadLevel(_selection == 0 ? true : false);
    }

    public void AddInitialEntityToPool(Entity carEntity)
    {
        initialLevelEntities.Add(carEntity);
        initLevelEntityCountRef++;
    }

    public void RemoveInitialEntityFromPool(Entity carEntity)
    {
        for (int i = 0; i < initialLevelEntities.Count; i++)
        {
            if (initialLevelEntities[i] == carEntity)
            {
                initialLevelEntities.RemoveAt(i);
                continue;
            }
        }
        
        if (initialLevelEntities.Count <= Mathf.Max(3, initLevelEntityCountRef / 4))
        {
            bIsInitialLevel = false;
            entitySpawner.EntitySpeed = entitySpeed;
        }
    }

    void clearInitialLevelEntities()
    {
        foreach (Entity ent in initialLevelEntities)
        {
            ent.transform.position += Vector3.right * 20.0f;
            ent.gameObject.SetActive(false);
        }

        initLevelEntityCountRef = 0;
    }
}
