﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour {

    public Transform myTransform, gunTransform, projectileSpawnPoint;
    public float gunRotSpeed, minDragDist, screenWidth = 0.0f, tapTime = 0.0f;
    [SerializeField] Vector3 firstTouchPosition;
    [SerializeField] bool touchToFire = false;
    //[SerializeField] TextMeshProUGUI ammoText;
    ProjectilePathIndicator pathIndicator;
    int availableAmmo = 0;
    bool showProjectilePath = false;
    private Vector3 laneSpacing;

    public int AvailableAmmo
    {
        get { return availableAmmo; }
        set { availableAmmo += value; UpdateAmmoText(availableAmmo); }
    }

    public float LaneSpacing
    {
        set => laneSpacing = new Vector3(value, 0.0f, 0.0f);
    }

	void Awake () {

        myTransform = this.transform;
        pathIndicator = gameObject.GetComponent<ProjectilePathIndicator>();
	}
    
    private void OnEnable()
    {
        InputManager.OnTouchDown += InputManager_OnTouchDown;
        InputManager.OnTouchMove += InputManager_OnTouchMove;
        InputManager.OnTouchUp += InputManager_OnTouchUp;

        if(pathIndicator == null)
        {
            pathIndicator = gameObject.GetComponent<ProjectilePathIndicator>();
        }
        pathIndicator.ShowProjectilePath(false);
        showProjectilePath = false;
        touchToFire = false;
    }

    private void OnDisable()
    {
        InputManager.OnTouchDown -= InputManager_OnTouchDown;
        InputManager.OnTouchMove -= InputManager_OnTouchMove;
        InputManager.OnTouchUp -= InputManager_OnTouchUp;
    }

    private void InputManager_OnTouchDown(Vector3 touchPos)
    {
        touchToFire = false;
        ProjectileSpawner.curProjectileSpawner.CanShoot = false;
        firstTouchPosition = touchPos;
        RaycastHit2D hit = Physics2D.Raycast(touchPos,-Vector2.up);

        touchToFire = true;
        if(Time.timeSinceLevelLoad <= tapTime + 0.5f)
        {
            touchToFire = false;
            ProjectileSpawner.curProjectileSpawner.ReturnAllProjectiles();
        }
        tapTime = Time.timeSinceLevelLoad;

        //if(hit.collider.CompareTag("Player"))
        //{
        //    touchToFire = true;
        //    if(Time.timeSinceLevelLoad <= tapTime + 0.5f)
        //    {
        //        touchToFire = false;
        //        ProjectileSpawner.curProjectileSpawner.ReturnAllProjectiles();
        //    }
        //    tapTime = Time.timeSinceLevelLoad;
        //    //pathIndicator.ShowProjectilePath(true);
        //}
        //else
        //{
        //    touchToFire = false;
        //}
    }

    private void InputManager_OnTouchUp(Vector3 touchPos)
    {
        Vector3 offset = firstTouchPosition - touchPos;
        Vector3 firstTouch = firstTouchPosition;
        firstTouchPosition = Vector3.zero; //reset first touch
        float dragDist = offset.sqrMagnitude;
        
        if(dragDist > minDragDist * minDragDist)
        {
            if(touchToFire == true)
            {
                touchToFire = false;
                ProjectileSpawner.curProjectileSpawner.Fire();
                pathIndicator.ShowProjectilePath(false);
                showProjectilePath = false;
                return;
            }
            //else
            //{
            //    float yDist = touchPos.y - firstTouch.y;
            //    float xDist = touchPos.x - firstTouch.x;

            //    if (yDist > 0.5f && yDist > xDist)
            //    {
            //        ProjectileSpawner.curProjectileSpawner.ReturnAllProjectiles();
            //        return;
            //    }

            //    if(myTransform.position.x <= -2.50f && xDist < 0.0f || myTransform.position.x >= 2.50f && xDist > 0.0f)
            //    {
            //        return;
            //    }

            //    if (xDist > 0.0f)
            //    {
            //        if ((myTransform.position.x + laneSpacing.x) >= screenWidth)
            //        {
            //            //hit the screen bounds
            //            return;
            //        }

            //        myTransform.position += laneSpacing;
            //    }
            //    else
            //    {
            //        if ((myTransform.position.x - laneSpacing.x) <= -screenWidth)
            //        {
            //            //hit the screen bounds
            //            return;
            //        }

            //        myTransform.position -= laneSpacing;
            //    }
            //}
            
        }

        touchToFire = false;
    }

    public void MovePlayerLeftRight(bool iSMoveRight)
    {
        if (iSMoveRight)
        {
            if ((myTransform.position.x + laneSpacing.x) >= screenWidth)
            {
                //hit the screen bounds
                return;
            }

            myTransform.position += laneSpacing;
        }
        else
        {
            if ((myTransform.position.x - laneSpacing.x) <= -screenWidth)
            {
                //hit the screen bounds
                return;
            }

            myTransform.position -= laneSpacing;
        }

    }

    private void InputManager_OnTouchMove(Vector3 touchPos)
    {
        if (touchToFire == false)
        {
            return;
        }
        

        Vector3 _rot = touchPos - firstTouchPosition;
        float _rotZ = Mathf.Atan2(_rot.y, _rot.x) * Mathf.Rad2Deg;
        gunTransform.rotation = Quaternion.Euler(0.0f, 0.0f, _rotZ - 270.0f);

        Vector3 offset = firstTouchPosition - touchPos;
        float dragDist = offset.sqrMagnitude;

        if(showProjectilePath == true)
        {
            return;
        }
        if (dragDist > minDragDist * minDragDist)
        {
            if(pathIndicator == null)
            {
                pathIndicator = gameObject.GetComponent<ProjectilePathIndicator>(); //safety
            }
            showProjectilePath = true;
            pathIndicator.ShowProjectilePath(true);
        }

    }

    private void Start()
    {
        if(gunTransform == null)
        {
            gunTransform = gameObject.transform.Find("GunGymbol");
        }
        
        //if (ammoText == null)
        //{
        //    ammoText = GameObject.Find("Canvas_Dynamic/AmmoText").GetComponent<TextMeshProUGUI>();
        //}

        UpdateAmmoText(availableAmmo);
    }

 //   void Update () {
        
 //       float _horiz = Input.GetAxis("Horizontal");

 //       if (_horiz != 0.0f)
 //       {
 //           gunTransform.Rotate(Vector3.forward, (-_horiz * gunRotSpeed) * Time.deltaTime);
 //       }
	//}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Enemy")) //disable player on collision with an "Enemy"
        {
            ParticleSystem _ps = Resources.Load<ParticleSystem>("CrashParticleEffect");
            Instantiate(_ps, myTransform.position, Quaternion.identity);
            Instantiate(_ps, other.transform.position, Quaternion.identity);
            AudioManager.audioManager.PlayAudioOneShot(AudioClipSelection.CarCrash);
            PlayerDied();
            other.gameObject.SetActive(false);
        }
    }

    void PlayerDied()
    {
        pathIndicator.ShowProjectilePath(false);
        showProjectilePath = false;
        ProjectileSpawner.curProjectileSpawner.ClearAllProjectiles();
        this.gameObject.SetActive(false);
        GameManager.curGameManager.PlayerDied();
    }

    void UpdateAmmoText(int val)
    {
        GameManager.curGameManager.UpdateAmmoText(val);
        pathIndicator.SetAmmoAvailable = availableAmmo > 0;
        //ammoText.text = val.ToString();
    }

    public void SetAvailableAmmo(int amt)
    {
        availableAmmo = amt;
    }
}
