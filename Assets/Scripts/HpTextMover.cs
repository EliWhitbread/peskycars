﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HpTextMover : MonoBehaviour {

    TextMeshPro textMesh;
    
    public void UpdateText(int amt)
    {

        if(textMesh == null)
        {
            //textMesh = gameObject.GetComponent<TextMesh>();
            textMesh = gameObject.GetComponent<TextMeshPro>();
        }

        textMesh.text = amt.ToString();
    }
}
