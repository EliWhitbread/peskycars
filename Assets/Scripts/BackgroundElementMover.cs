using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundElementMover : MonoBehaviour
{
    [SerializeField]float ySpeed;

    private Transform myTransform;

    private void Awake()
    {
        myTransform = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        myTransform.Translate(Vector3.down * ySpeed * Time.deltaTime);
    }
}
